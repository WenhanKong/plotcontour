Install
=======

**Install Python**  

1. Follow Python installation process on: https://realpython.com/installing-python/  

    
    a.  Go to https://www.python.org/downloads/windows/  

    |

    b.	Click “Latest Python 3 Release”. (currently 3.7.4)

        .. image:: /images/install/1.png

|
  
    c.	Download Windows x86-64 executable installer 

        .. image:: /images/install/2.png

|
  
    d.	Open installer and Click “Add Python 3.X to PATH”

        .. image:: /images/install/3.png
  
|
  
    e.	Click “Install Now”

    f.	Complete installation.

    

    

    



2. Make sure Python has been added to PATH variable. How: (https://datatofish.com/add-python-to-windows-path/).  
    
    At the command line:: 

        python --version


3.	Use pip to Install external packages needed for the Python script. (what is pip and how to use it: https://realpython.com/what-is-pip/#getting-started-with-pip).   

    - For installing external packages, at command line::

            pip install matplotlib numpy pandas

    OR

    - if you have a copy of requirements.txt, at command line:: 
 
            pip install -r \path\to\requirements.txt

    Both commands automatically install the latest version of matplotlib, numpy and pandas with their dependencies. 

|
|

**After successful installation, please proceed to "Getting Started".**

    