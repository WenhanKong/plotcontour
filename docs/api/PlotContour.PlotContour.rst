PlotContour
===========

.. currentmodule:: PlotContour

.. autoclass:: PlotContour
   :show-inheritance:

   .. rubric:: Methods Summary

   .. autosummary::

      ~PlotContour.create_folder
      ~PlotContour.find_csv_filenames
      ~PlotContour.plot_save
      ~PlotContour.read_data
      ~PlotContour.run

   .. rubric:: Methods Documentation

   .. automethod:: create_folder
   .. automethod:: find_csv_filenames
   .. automethod:: plot_save
   .. automethod:: read_data
   .. automethod:: run
