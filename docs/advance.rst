Advance
===============

**Advancing PlotContour Script by Using API**

        1. Create a new PlotContour instance, then call run_multiprocess method to start. 
           Without specifing any parameters, PlotContour will use the default one. ::  
        
            new_task = PlotContour()                                                     
            new_task.run_multiprocess() 

         Run code above gives the result:  


        +------------------------------------------------------+-----------------------+
        |                   Contour Figures                    |       Groupby         |
        +======================================================+=======================+
        | .. image:: /images/1/data_['StressTime']=0.png       | StressTime=0          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/1/data_['StressTime']=1.png       | StressTime=1          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/1/data_['StressTime']=20000.png   | StressTime=20000      |
        +------------------------------------------------------+-----------------------+


        2. Changing ADCSample_names affects which columns will be drawn: ::

            new_task = PlotContour(ADCSample_names = ['ADCSample_Calc'])
            new_task.run_multiprocess()                                                                              
    

        +------------------------------------------------------+-----------------------+
        |                   Contour Figures                    |       Groupby         |
        +======================================================+=======================+
        | .. image:: /images/2/data_['StressTime']=0.png       | StressTime=0          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/2/data_['StressTime']=1.png       | StressTime=1          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/2/data_['StressTime']=20000.png   | StressTime=20000      |
        +------------------------------------------------------+-----------------------+


        3. Swithing x and y: ::

            new_task = PlotContour(y='idx', x='OffTrack_nm')
            new_task.run_multiprocess()


        +------------------------------------------------------+-----------------------+
        |                   Contour Figures                    |       Groupby         |
        +======================================================+=======================+
        | .. image:: /images/3/data_['StressTime']=0.png       | StressTime=0          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/3/data_['StressTime']=1.png       | StressTime=1          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/3/data_['StressTime']=20000.png   | StressTime=20000      |
        +------------------------------------------------------+-----------------------+

        4. Change figure size: ::

            new_task = PlotContour(figsize=(10,5))
            new_task.run_multiprocess()

        +------------------------------------------------------+-----------------------+
        |                   Contour Figures                    |       Groupby         |
        +======================================================+=======================+
        | .. image:: /images/4/data_['StressTime']=0.png       | StressTime=0          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/4/data_['StressTime']=1.png       | StressTime=1          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/4/data_['StressTime']=20000.png   | StressTime=20000      |
        +------------------------------------------------------+-----------------------+

        5. Change color pattern: ::

            new_task = PlotContour(figsize=(20,5), colormap='rainbow')
            new_task.run_multiprocess()

        +------------------------------------------------------+-----------------------+
        |                   Contour Figures                    |       Groupby         |
        +======================================================+=======================+
        | .. image:: /images/5/data_['StressTime']=0.png       | StressTime=0          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/5/data_['StressTime']=1.png       | StressTime=1          |
        +------------------------------------------------------+-----------------------+
        | .. image:: /images/5/data_['StressTime']=20000.png   | StressTime=20000      |
        +------------------------------------------------------+-----------------------+
