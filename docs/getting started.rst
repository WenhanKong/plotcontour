Getting Started
===============

**Using PlotContour.exe**

Using PlotContour.exe does not require installations of Python and other external packages. At the mean time you are not allowed to specify any parameters. All figures are drawn using default configurations.

1. Put the exe file with data files under the same directory.
    .. image:: /images/gettingstarted/5.png
        :align: center

|

2. Double-click to run.
    \
3. A folder/Folders with same name as data files will be generated under the current directory. The folder/Fodlers contains all the figures.
    .. image:: /images/gettingstarted/4.png
        :align: center

|
|

**Using PlotContour Script**

1. Assuming you have successfully installed Python and other external packages. Now you can try to run the script with Python:
    *	Put the script with data files under the same directory. Right-click the script, choose Edit with IDLE (your python version)
        
        .. image:: /images/gettingstarted/1.png 

    *WARNING: the script has been only tested on Python 3.7; Python 2 is not compatible with python 3 and is no longer supported by python.org. If Python 2 is the only option on the list, please follow steps above to install python 3 properly.*

|

2. After IDLE window is opened, 
    *	Click Run, then click Run Module.  

    OR  

    *	press F5.  

    .. image:: /images/gettingstarted/2.png 
        :align: center

|

3. A Python shell window will pop up. After the job is done, the shell will show how many files are generated and total time usage. After that you are free to close both Shell window and IDLE window.
    .. image:: /images/gettingstarted/3.png
        :align: center

|

4. A folder/Folders with same name as data files will be generated under the current directory. The folder contains all the figures.
    .. image:: /images/gettingstarted/4.png
        :align: center

|


        

