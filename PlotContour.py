#!/usr/bin/env python
# coding: utf-8

#Author: Wenhan Kong
#version: 0.1.4

####################################################################################
## PlotContour automatically search all files ending with .csv under              ##
## the current directory. Then generates a folder for each file and corresponding ##
## tricontour figures. At last save them under the directory just created.        ##
####################################################################################

#import matplotlib
#matplotlib.use('TkAgg')
#import pandas_profiling as pdp

import numpy as np
import matplotlib.pyplot as plt
import os
import shutil
import tempfile
import time
import pandas as pd 
import multiprocessing as mp
from tqdm import tqdm


class PlotContour:

    def __init__(self, x='DownTrack_nm', y='OffTrack_nm', suffix='.csv', 
                 ADCSample_names=['ADCSample_Calc', 'No0_Normalize'], 
                 groupby=['StressTime'], figsize=(10,5), colormap='bwr', dpi=200):

        """
        PlotContour class

        :param x: Column name for X axis, X specifies x-axis with corresponding labels. 
        :type x: str
        :param y: Column name for Y axis, Y specifies y-axis with corresponding labels. 
        :type y: str
        :param suffix: File type suffix
        :type suffix: str
        :param ADCSample_names: Column names for Z axis. ADCSample_names takes in a list of string. PlotContour takes each string in the list and read corresponding columns and draw the graph accordingly. 
        :type ADCSample_names: [str]
        :param groupby: Column names for grouping data. d.	Groupby takes a list of string as parameter. Normally the list contains only one element. List length larger than 1 has not been tested yet. Groupby determines which column is used to separate data. It also affects filenames and figure titles. 
        :type groupby: [str]
        :param figsize: Determines size of figures. figsize[0] determines legnth, whereas figsize[1] determines width.
        :type figsize: tuple(int, int)
        :param colormap: Determines the color arrangement in color bars. Choices refer to  https://matplotlib.org/3.1.1/gallery/color/colormap_reference.html
        :type colormap: str
        :param dpi: Determines dpi of figures drawn. Higher dpi means higher resolution as well as longer saving time, and vice versa. 
        :type dpi: int
        """

    # curr_path is used to save figures under the same dir with csv files.
        self.curr_dirpath = os.getcwd()
    #
        self.x = x
    #
        self.y = y
    # suffix specifies file type.
        self.suffix = suffix
    # ADCSample_names specifies which columns are needed as z axis.
        self.z = ADCSample_names
    # groupby specifies which column is used as group by parameter. StressTime is set as default.
        self.groupby=groupby
    #
        self.figsize = figsize

    #
        self.colormap = colormap
    # matplolib default dpi is 100, which is relative low. Here we set it to 200.
        self.dpi = dpi


    #Find and add all files ending with .suffix
    def find_csv_filenames(self):
        """
        Find all file ending with suffix under current directory.

        :returns: returns a list of filenames under current directory ending with suffix
        :rtype: [suffix]
        """
        all_filenames = os.listdir(self.curr_dirpath)
        return [ filename for filename in all_filenames if filename.endswith(self.suffix) ]


    def create_folder(self, filename):
        """
        Create a folder to store figures per csv file.

        :param filename: specifies folder name (Str)
        :type filename: str
        :returns: Absolute path for new created folder
        :rtype: str
        """
        # create a folder for the current data file to store all related figures 
        destination_path = self.curr_dirpath + '\\' + filename  
        try:
            if os.path.exists(destination_path):
                # overwrite if the folder already exists
                shutil.rmtree(destination_path)
                # wait 500 milliseconds for os operations
                time.sleep(.500)
            os.makedirs(destination_path)
        except Exception as e:
            print(e)
        return destination_path


    def read_data(self, file):
        """
        load data from csv to pandas.DataFrame object. Group dataframe with groupby. 

        :param file: specifies file name
        :type file: str
        :returns: grouped pandas.DataFrame object. 
        :rtype: DataFrame
        """
        #read data from one file and group the data by StressTime
        data = pd.read_csv(self.curr_dirpath+'\\'+ file)
        data_grouped = data.groupby(by=self.groupby)
        return data_grouped

    def plot_save(self, raw_filename):
        # draw and save figures, numbers depend on the StressTime column
        """
        plot figures from data_grouped and save figures under destination.

        :param data_grouped: grouped pandas.DataFrame object. (DataFrame)
        :type data_grouped: pandas.DataFrame
        :param filename: Folder name created by creat_folder to store figures
        :type filename: str
        :param destination: Absolute path of Folder created by create_folder
        :type destination: str

        """
        try:
            data_grouped = self.read_data(raw_filename)
            for name, grouped in data_grouped:
                if name != 0:
                    fig, axes = plt.subplots(1,len(self.z),figsize=((len(self.z)+1)*self.figsize[0], self.figsize[1]))
                    #used to iterate ADCSample_names
                    start_index = 0
                    #store images for drawing colorbar
                    images = []
                    #if ADCSample_names length is one, axes is not a list object thus not iterable. Need to convert axes to a list.
                    if len(self.z)==1:
                        axes = [axes]
                    for ax in axes:
                        # tricontourf requires a dataframe without any NaN value. dropna() is used to ignore all rows with NaN.
                        grouped_without_NaN = grouped.dropna(subset=[self.z[start_index]])
                        # draw
                        if self.z[start_index] == 'No0_Normalize':
                            im = ax.tricontourf(grouped_without_NaN[self.x], grouped_without_NaN[self.y], 
                                                grouped_without_NaN[self.z[start_index]], cmap=self.colormap,
                                                levels=[(x/10) for x in range(1, 10)], extend='both'
                                            )
                            images.append(im)
                        else:
                            im = ax.tricontourf(grouped_without_NaN[self.x], grouped_without_NaN[self.y], 
                                                grouped_without_NaN[self.z[start_index]], cmap=self.colormap,
                                                levels=[x for x in range(-31, 29, 5)], extend='both'
                                            )
                            images.append(im)
                        #images.append(im)

                        # set titles and labels
                        ax.title.set_text('{}={}, {}'.format(self.groupby, name, self.z[start_index]))
                        ax.set_xlabel(self.x)
                        ax.set_ylabel(self.y)
                        # Move to the next element of ADCSample_names
                        start_index += 1

                    #draw color bar per subplot
                    for i in range(len(self.z)):
                        fig.colorbar(images[i], ax=axes[i], extend='both')
                    
                    try:
                        filename = raw_filename.split('.')[0]
                        destination = self.create_folder(filename)
                        # save figure to destination foler with StreeTime value as addtional suffix.
                        plt.savefig(destination + '\\' + filename + '_{}={}.png'.format(self.groupby, name), dpi=self.dpi)
                    except Exception as e:
                        print(e)
                        print("Error saving your figure\n")
                        raise
        except:
            raise
        finally:
            # refresh plt
            plt.clf()
            plt.cla()
            plt.close()


    def run(self):
        
        """
        Main function of PlotContour class. It uses multi-process to accelarate plot&save. mp.Process creates 
        a process pool object. Iterate the pool to start each process and wait until all ends. After job
        is done, total files read and total time usage will be displayed.

        :returns: None
        """

        print('PlotContour Starting ... ')

        try:
            filenames = self.find_csv_filenames()
            if not filenames:
                return
            task_length = len(filenames)
            print("Total files: {}".format(task_length))

            with mp.Pool() as pool:
                # ascii needs to set to be True, or windows interprets unicode incorrectly.
                for _ in tqdm(pool.imap_unordered(self.plot_save, filenames), total=task_length, ascii=True):
                    pass

        except:
            raise
        finally:
            print("... PlotContour Finished ")
        


if __name__ == "__main__":

    #freeze_support elimates infinite loop running executable file which uses multi-processes.
    mp.freeze_support()
    new_task = PlotContour()

    #then = time.time()
    new_task.run()
    # now = time.time()
    # print('Run with nomal method took: {}'.format(now-then))

    # then = time.time()
    # new_task.run_multiprocess()
    # now = time.time()
    # print('Run with multiprocess took: {}'.format(now-then))




